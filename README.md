Configuracion Inicial

Git global setup
git config --global user.name "Marco Rodriguez"
git config --global user.email "marcorodriguezvar@gmail.com"

Create a new repository
git clone https://gitlab.com/s53g13-TiendaVirtual3/s53g13-tienda-virtual.git
cd s53g13-tienda-virtual
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

Push an existing folder
cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/s53g13-TiendaVirtual3/s53g13-tienda-virtual.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push an existing Git repository
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/s53g13-TiendaVirtual3/s53g13-tienda-virtual.git
git push -u origin --all
git push -u origin --tags
